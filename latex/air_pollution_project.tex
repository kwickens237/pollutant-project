\documentclass[12pt]{report}

% Packages
\usepackage[utf8]{inputenc}
\usepackage{fullpage} % Package to use full page
\usepackage{parskip} % Package to tweak paragraph skipping
\usepackage{tikz} % Package for drawing
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{authblk}
%\usepackage{natbib}
\usepackage{multirow}
\usepackage{float}
\usepackage{pythonhighlight} % Package for Python code highlighting
\usepackage{listings} % Package for R code highlighting
\usepackage{xcolor}
\usepackage[font=small,labelfont=bf]{caption}
\usepackage{cleveref}
\usepackage{booktabs}


% Convenient Formatting
\setlength{\textheight}{9in}
\setlength{\topmargin}{-0.600in}
\setlength{\headheight}{0.2in}
\setlength{\headsep}{0.250in}
\setlength{\footskip}{0.5in}
\flushbottom
\setlength{\textwidth}{6.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\columnsep}{2pc}
\setlength{\parindent}{1em}

% R highlighting
\lstset{language=R,
    basicstyle=\small\ttfamily,
    stringstyle=\color{purple},
    otherkeywords={0,1,2,3,4,5,6,7,8,9},
    morekeywords={TRUE,FALSE},
    deletekeywords={data,frame,length,as,character},
    keywordstyle=\color{blue},
    commentstyle=\color{purple},
}

% UDFs from Lajos
\def\beq{\begin{equation}}
\def\eeq{\end{equation}}
\newcommand\bbeta{\mbox{\boldmath${\eta}$}}

% File path for images
\graphicspath{ {images/} }

% Title
% \title{Functional Data Analysis of Air Pollutants in Salt Lake County}
% \author{Kali Wickens}
% \date{\today}

% \title{
% {Functional Data Analysis of Air Pollutants in Salt Lake County}\\
% {\large University of Utah}\\
% }
% \author{Kali Wickens}
% \date{December 2021}

\begin{document}

\input{paper/titlepage.tex}

\tableofcontents

\pagenumbering{roman}

\chapter*{Abstract}
\addcontentsline{toc}{chapter}{Abstract}
    \input{paper/abstract.tex}

\chapter*{Acknowledgements}
\addcontentsline{toc}{chapter}{Acknowledgements}
    \input{paper/acknowledgements.tex}

\listoffigures
\addcontentsline{toc}{chapter}{List of Figures}

\listoftables
\addcontentsline{toc}{chapter}{List of Tables}


\chapter{Introduction}
\pagenumbering{arabic}

There is nothing that can unite all humans more than the air we breathe.
The air around us is something that is often taken for granted, something unseen and neglected as we go about our daily lives.
Yet, our air, or rather, what is in it affects our health and well being.

The US Environmental Protection Agency (EPA) monitors ambient air quality to assess and measure pollutants that affect our health and environment.
While the EPA began tracking monitoring data as far back as 1957, 1980 was the beginning of nationally consistent standards and is the first year used for tracking trends. 
Six air pollutants, called criteria pollutants, are part of the daily air quality index report.
These are Ozone, Sulfur Dioxide (SO$_{2}$), Nitrogen Dioxide (NO$_{2}$), Carbon Monoxide (CO), Particulate Matter (PM$_{10}$ and PM$_{2.5}$), and Lead (Pb) (EPA: Outdoor Air Quality Basic Information).

In the Salt Lake Valley, we are not only exposed to these pollutants, but we also have the misfortune of seeing these linger in the valley for days at a time due to specific weather patterns.
The weather pattern, inversion, causes these pollutants to reach higher concentrations as they remain trapped in the valley, ultimately leading to reduced air quality for everyone and potentially dangerous conditions for vulnerable members of the population (Utah DEQ: Pollutants Information).

This paper aims to analyze the trends of these three of these pollutants: CO, PM$_{2.5}$, and SO$_{2}$, for Salt Lake County over the decades that the EPA has recorded these values.
While these data are discrete observations, they belong to an underlying continuous curve that represents the presence of a pollutant in the ambient air.
Thus, functional data analysis will be used on this recorded data for monitoring stations in the Salt Lake Valley to compare across the years and against different cities in the US.

\chapter{Functional data}

    \section{Some theoretical results on functional time series}
        \input{paper/functional_theory.tex}

        Given this theoretical explanation, we will now apply this to the EPA criteria pollutant use case.

\chapter{EPA Criteria Pollutants}

    \section{EPA pollutant summary data}
        \input{paper/epa_pollutant_data.tex}

    \section{Carbon Monoxide}
        \input{paper/co_data.tex}

    \section{Particulate Matter 2.5}
        \input{paper/pm25_data.tex}

    \section{Sulfur Dioxide}    
        \input{paper/SO2_data.tex}

    \section{Functional data for pollutants}
        \input{paper/pollutant_functions.tex}

\chapter{FANOVA}
    \input{paper/fanova.tex}

\chapter*{Conclusion}
\addcontentsline{toc}{chapter}{Conclusion}
    \input{paper/conclusion.tex}


\chapter*{Appendix}
\addcontentsline{toc}{chapter}{Appendix}
    \section*{\texttt{R} code for FANOVA}
        \input{paper/pfanova_code.tex}
    \section*{Tables of observation counts per monitoring site}
        \input{paper/pm25_site_num_counts.tex}
        \input{paper/co_site_num_counts.tex}
        \input{paper/so2_site_num_counts.tex}


\medskip

\begin{thebibliography}{99}

    \bibitem{hps} H\"ormann, S.,\ Pfiler, B.\ and Stadlober, E.: {\it Analysis and Prediction of Particulate Matter PM10 for the Winter Season in Graz.} Austrian Journal of Statistics 34(4), 307 -- 326, 2005.

    \bibitem{hk} Horv\'ath, L.\ and Kokoszka, P.: {\it Inference for Functional Data with Applications.}  Springer, New York, 2012.

    \bibitem{hr} Horv\'ath, L.\ and Rice, G.: {\it Testing Equality of Means When the Observations are from Functional Time Series.} Journal of Time Series Analysis 36, 84 --108, 2015.

    %\bibitem{serf} Serfling, R.J.: {\it Approximation Theorems of Mathematical Statistics.} Wiley, 1980.
    \bibitem{korebo} Kokoszka, P.\ and  Reimherr, M.: {\it Introduction to Functional Data Analysis.}  Chapman and Hall/CRC, 2017.

    \bibitem{ramsay} Ramsay, J.,\ Hooker, G.\ and Graves, S.: {\it Functional Data Analysis with R and MATLAB.} Springer, New York, 2009.

    \bibitem{outdoor} U.S. Environmental Protection Agency: Outdoor Air Quality Basic Information. Retrieved October 16, 2021, from \url{https://www.epa.gov/outdoor-air-quality-data/air-data-basic-information#concepts}

    \bibitem{airdata} U.S. Environmental Protection Agency: Annual Summary Data. Retrieved October 13 and November 3, 2021, from \url{https://aqs.epa.gov/aqsweb/airdata/download_files.html}

    \bibitem{sites} U.S. Environmental Protection Agency: Interactive Map of Air Quality Monitors. Retrieved October 27, 2021, from \url{https://www.epa.gov/outdoor-air-quality-data/interactive-map-air-quality-monitors}

    \bibitem{udeq} Utah Department of Environmental Quality: Pollutants Information. Retrieved November 4, 2021, from \url{https://deq.utah.gov/pollutants/pollutants-index}

\end{thebibliography}
\addcontentsline{toc}{chapter}{References}

\chapter*{Code Repository}

Below is a link to the Gitlab repository with all of the data, code, and figures contained in this paper.

\url{https://gitlab.com/kwickens237/pollutant-project/-/tree/master}

\end{document}