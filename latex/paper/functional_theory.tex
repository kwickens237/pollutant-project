If we have large dimensional observations, working with them as high dimensional vectors, might not be suitable in statistical analysis. 
Several statistical analysis is based on the empirical covariance matrix and in several applications the empirical covariance matrix is close to be singular. 
This causes instability in calculations and the inverse of the empirical covariance has large statistical as well as numerical error. 
It is natural to model a data as functions, observed them at finitely many points. 
We assume that we have densely observed functions. 
Practically, the function can be replaced with the observations at finitely many points. The other motivation for functional data is even simpler. 
Certain quantities are functions. For example, temperature, blood pressure, stock prices, air pollution always exist and we can observe them densely. 
It is usually just physical limitation how densely we can observe them. 
Instead of having an extremely long function, we cut the observations using time periods. For example, blood pressure, temperature and pollution curves are given daily. 
This takes into account the periodicity of the these quantities. 
In other cases, the temperature data are given as curve using daily observations. 
In this case usually just daily 1--3 observations are available, especially in case of older data. 
These motivating examples are discussed in Horv\'ath and Kokoszka (2012) and Kokoszka and Reimherr (2017). 
Based on the books of Horv\'ath and Kokoszka (2012) and Kokoszka and Reimherr (2017) we discuss some theoretical results which are used in this project.



We say that $\{ X(t;\omega), 0\leq t \leq 1\}$ is a random function if for every $t_1, t_2, \ldots, t_M$ is an M dimensional random variable and for every $\omega$ it is a function on $[0,1]$. 
(We note that we can always omit a zero set of $\omega$'s). 
It is standard in probability theory that in the notation the dependence on $\omega$ is omitted and we just write $X(t)$ for a random function. 
The most often used model assumes some regularity conditions on $X(t)$. 
The most popular one says that $X(t)\in L_2$, i.e.\ almost all realization of $X(t)$ is square integrable, meaning
\beq\label{f1}
P\left\{\omega:\;\;\int_0^1 X^2(t;\omega)dt<\infty   \right\}=1.
\eeq
During the calculations and to use principal component analysis we also assume
\beq\label{f2}
EX^2(t)dt<\infty \;\;\mbox{for all}\;\;t\in [0,1]\;\;\mbox{and}\;\;\int_0^1 EX^2(t)dt<\infty.
\eeq
We note that \eqref{f2} implies \eqref{f1}. If \eqref{f2} holds we can define the mean function
$$
\mu(t)=EX(t)
$$
and the covariance function
$$
C(t,s)=E(X(t)-\mu(t))(X(s)-\mu(s)).
$$
The existence of $\mu(t)$ and $C(t,s)$ follows from \eqref{f2}, since by the Cauchy--Schwartz inequality
$$
|EX(t)|\leq E|X(t)|\leq (EX^2(t))^{1/2}
$$
and
\begin{align*}
|C(t,s)|=|E(X-\mu(t))(X(s)-\mu(s))|\leq (E(X(t)-\mu(t))^2)^{1/2}(E(X(s)-\mu(s))^2)^{1/2}<\infty.
\end{align*}
Thus we get
\begin{align*}
\int_0^1\int_0^1 C^2(t,s)dtds&\leq \int_0^1 (E(X(t)-\mu(t))^2)(E(X(s)-\mu(s))^2)dtds\\
&=\left(\int_0^1(E(X(t)-\mu(t))^2)dt\right)^2<\infty.
\end{align*}
We also note that since $C(t,s)$ is a covariance function it is symmetric and non negative definite. 
By non negative definite property we mean that
$$
\int_0^1\int_0^1z(t)C(t,s)z(s)dtds\geq 0 % typo removed ds
$$
for all $z(t)\in L_2([0,1])$. 
We observe
\begin{align*}
\int_0^1\int_0^1&z(t)C(t,s)z(s)dtds\\ % typo removed ds
&=\int_0^1\int_0^1 z(t)E[(X(t)-\mu(t))(X(s)-\mu(s))]z(s)dtds\\ % typo changed second t to s
&\leq \int_0^1\int_0^1 |z(t)|(E[(X(t)-\mu(t))]^2)^{1/2}(E[(X(s)-\mu(s))])^{1/2}|z(s)|dtds\\ % typo changed second t to s
&= \int_0^1\int_0^1 \{|z(s)|(E[(X(t)-\mu(t))]^2)^{1/2}\}\{(E[(X(t)-\mu(t))])^{1/2}|z(t)|\}dtds\\
&\leq \left(\int_0^1\int_0^1 z^2(s)E[(X(t)-\mu(t))]^2dtds\right)^{1/2}\left(\int_0^1\int_0^1 z^2(t)|E[(X(s)-\mu(s))]^2dtds\right)^{1/2}\\
&=\left(\int_0^1 z^2(s)\right)\left( \int_0^1 E[(X(t)-\mu(t))]^2dt  \right)<\infty.
\end{align*}
Hence
$$
\left|\int_0^1\int_0^1z(t)C(t,s)z(s)dsdtds\right|<\infty.
$$
Now we can write
\begin{align*}
\int_0^1\int_0^1 z(t)C(t,s)z(s)dtds&=E\left(\int_0^1 z(t)(X(t)-\mu(t))dt\right)\left(\int_0^1 z(s)(X(s)-\mu(s))ds\right)\\ % typo removed ds
&=E\left(\int_0^1 z(t)(X(t)-\mu(t))dt\right)^2\geq 0.
\end{align*}
This result show that $C(t,s)\in L_2([0,1]\times[0,1])$. 
Hence we can define an integral operator  on the space $L_2([0,1]$. Let $z(t)\in L_2([0,1])$ and define
$$
w(t)=\int_0^1 C(t,s)z(s)ds.
$$
It is easy to see that $w\in L_2$:
\begin{align*}
\int_0^1 w^2(t)dt&=\int_0^1 \left(\int_0^1 C(t,s)z(s)ds\right)^2dt\\
&\leq \int_0^1\left( \int_0^1 C^2(t,s)ds\int_0^1z^2(s)ds      \right)dt\\
&=\int_0^1\int_0^1C^2(t,s)dtds \int_0^1z^2(s)ds<\infty.
\end{align*}
Hence we have a mapping from $L_2$ to $L_2$.

Since $C(t,s)$ is square integrable, symmetric and non--negative definite, there are $\lambda_1\geq \lambda_2\geq \ldots\geq 0$ (eigenvalues) and the corresponding functions $\phi_1(t), \phi_2(t), \ldots$ (eigenfunctions)
such that
$$
\lambda_i\phi_i(t)=\int_0^1 C(t,s)\phi_i(s)ds, \quad\quad i=1,2,\ldots
$$
We note
\beq\label{phi1}
\int_0^1\phi_i(t)\phi_j(t)=0\quad\quad \mbox{if}\;\;  i\neq j
\eeq
and we can assume
\beq
\int_0^1\phi^2_i(t)dt=1.\label{ph1}
\eeq
If there is no tie between the eigenvalues, then the proof of \eqref{ph1} is simple,
\begin{align*}
\int_0^1\phi_i(t)\phi_j(t)dt&=\int_0^1\phi_i(t)\frac{1}{\lambda_j}\left(\int_0^1C(t,s)\phi_j(s)ds\right)dt\\
&=\frac{1}{\lambda_i}\int_0^1\int_0^1\phi_i(t)C(t,s)\phi_j(s)dtds
\end{align*}
and by the same argument
$$
\int_0^1\phi_i(t)\phi_j(t)dt=\frac{1}{\lambda_j}\int_0^1\int_0^1\phi_i(t)C(t,s)\phi_j(s)dtds,
$$
using the symmetry of $C(t,s)$. 
Hence $\lambda_i=\lambda_j$ but we assumed that there are no ties. 
If there are ties, the eigenfunctions belonging to the repeated eigenvalue are orthonormal. 
If there is $d\geq 1$ such that $\lambda_{d+1}=0$, then $X(t)$ is $d$ dimensional.

According to the spectral theorem
$$
C(t,s)=\sum_{i=1}^\infty \lambda_i\phi_i(t)\phi_i(s).
$$
The convergence above is in $L_2$ sense,
$$
\int_0^1\int_0^1\left( C(t,s)- \sum_{i=1}^M \lambda_i\phi_i(t)\phi_i(s) \right)^2dtds\to 0,\quad\quad\mbox{as}\;\;M\to\infty.
$$

According to the Karhunan--Lo\'eve expansion
$$
X(t)-\mu(t)=\sum_{i=1}^\infty\lambda_i^{1/2} \xi_i\phi_i(t),
$$
where the random variables $\xi_i, i\geq 1$ satisfy
\beq\label{xide1}
E\xi_i=0,\;\; E\xi_i\xi_j=0 \;\;\;\mbox{and}\;\;E\xi_i^2=1.
\eeq
The variables $\xi_i$ are defined as
\beq\label{xide2}
\xi_i=\frac{1}{\lambda_i^{1/2}}\int_0^1(X(t)-\mu(t))\phi_i(t)dt.
\eeq
(If $\lambda_i=0$, then there is no corresponding $\xi_i$.) The properties in \eqref{xide1} are immediate consequence of 
\eqref{xide2}, 
$$
E\xi_i=\frac{1}{\lambda_i^{1/2}}\int_0^1E(X(t)-\mu(t))\phi_i(t)dt=0,
$$
$$
 E\xi_i\xi_j=\frac{1}{(\lambda_i\lambda_j)^{1/2}}\int_0^1\int_0^1\phi_i(t)C(t,s)\phi_j(s)dtds.
$$
For the sake of simplicity we assume that $\mu(t)=0$ from now on.

We say that $\{X(t), 0\leq t \leq 1\}$ is a Gaussian process if for any $t_1, t_2, \ldots, t_d$, the joint distribution of the vector $(X(t_1), X_(t_2), \ldots, X(t_d))$ is $d$ dimensional normal. 
Linear combinations of the coordinates of a multivariate normal random vector is normal, it follows from \eqref{xide1} and \eqref{xide2} that in case of a Gaussian process, the random variables $\xi_1, \xi_2, \ldots, $ are independent and identically distributed standard normal random variables.

The functional principle component analysis means that we use the approximation 
$$
X(t)\approx \mu(t)+\sum_{\ell=1}^M\lambda_i^{1/2}\xi_i\phi_i(t)
$$
or 
$$
X(t)\approx \mu(t)+\sum_{\ell=1}^M\left(\int_0^1(X(s)-\mu(s))\phi_i(s)ds  \right)\phi_i(t).
$$

Let $X_1(t), X_2(t), \ldots, X_N(t)$ be independent and identically distributed, our observations for $X(t)$. We estimate the mean with 
$$
\hat{\mu}_N(t)=\frac{1}{N}\sum_{i=1}^N X_i(t)
$$
and the covariance function with
$$
\hat{C}_N(t,s)=\frac{1}{N}\sum_{i=1}*N(X_i(t)-\hat{\mu}_N(t))(X_i(s)-\hat{\mu}_N(s)).
$$
It is proven in  Horv\'ath  and Kokoszka (2012)  and Kokoszka  and Reimherr (2017)  that 
$$
\int_0^1\left(\hat{\mu}_N(t)-\mu(t) \right)^2dt\stackrel{P}{\to} 0
$$
and
$$
\int_0^1\int_0^1 \left(\hat{C}_N(t,s)-C(t,s)\right)^2dtds\stackrel{P}{\to} 0.
$$
The function $\hat{C}_N(t,s)$ is symmetric and non negative definite and square integrable. 
Thus there are functions 
$\hat{\lambda}_1, \hat{\lambda}_2, \ldots, \hat{\lambda}_N$ and corresponding orthonormal functions $ \hat{\phi}_1(t), \hat{\phi}_2(t), \ldots, \hat{\phi}_N(t)$ such that 
$$
\hat{\lambda}_i\hat{\phi}_i(t)=\int_0^1\hat{C}_N(t,s)\hat{\phi}_i(s)ds, \quad\quad1\leq i \leq N.
$$
(The function $\hat{C}_N(t,s)$ is at most $N$ dimensional, since it is spanned by $N$ functions, $X_1(t), X_2(t), \ldots, X_N(t)$.) 
We can use 
$\hat{\lambda}_1, \hat{\lambda}_2, \ldots, \hat{\lambda}_N$ as the estimators for the eigenvalues and $ \hat{\phi}_1(t), \hat{\phi}_2(t), \ldots, \hat{\phi}_N(t)$ are our estimates for the eigenfunctions. 
Even if the eigenvalues are distinct the eigenfunction is determined only up to the sign, the $\hat{\phi}_i(t)$ is close to $\phi_i(t)$ or $-\phi_i(t)$. 
Our procedures do not change if $\phi_i(t)$ or $-\phi_i(t)$ is estimated, the procedures we use the present work are invariant for the signs of the eigenfunctions. 
Let
$$
\hat{\eta}_{i,j}=\int_0^1(X_i(t)-\hat{\mu}_N(t))\hat{\phi}_j(t)dt, \quad \quad 1\leq i \leq N.
$$
The FPCA uses the approximation 
$$
X_i(t)\approx \hat{X}_i(t)=\hat{\mu}_N(t)+\sum_{j=1}^N\hat{\eta}_{i,j}\hat{\phi}_j(t), \quad\quad 1\leq i \leq N.
$$
The statistical analysis uses the functions $\hat{\mu}_N, \hat{X}_1(t), \hat{X}_2(t), \ldots, \hat{X}_N(t)$ or directly the score vectors 
$\hat{\bbeta}_i=(\hat{\eta}_{i,1}, \hat{\eta}_{i,2}, \ldots, \hat{\eta}_{i,M}), 1\leq i \leq N$. 
Thus the infinite dimensional problem is reduced to a finite $M$ dimensional problem. The parameter $M$ is chosen as 
\beq\label{crow}
\frac{\lambda_1+\lambda_2+\ldots+\lambda_M}{\displaystyle \sum_{i=1}^\infty\lambda_i}\approx .75 \;\;(\mbox{or}\;\;.8, .9).
\eeq
The examples in in  Horv\'ath  and Kokoszka (2012)  and Kokoszka  and Reimherr (2017)  use $M$ between 3 and 9. 
In sample applications we replace the $\lambda_i$'s with the corresponding estimators. The infinite sum can be easily estimated since
$$
\int_0^1\int_0^1\hat{C}_N^2(t,s)dtds\stackrel{P}{\to}\sum_{i=1}^\infty\lambda_i.
$$