The EPA publishes yearly summary data on the criteria air pollutants to the government website (EPA: Annual Summary Data).
This data is summarized for all of the monitoring sites across the US daily; data for just the Salt Lake Valley can be found by filtering on \texttt{state\_code = `49'} and \texttt{county\_code = `35'}.
Each pollutant has a yearly summary going back as far as 1980, each stored in a separate \texttt{.csv} file.

To collect the data, I downloaded each yearly summary file and wrote a script to merge, clean, and filter the \texttt{.csv} files into a single dataset.

\begin{python}
import pandas as pd

file_path = directory

# This function takes the file name, state code, and county code 
# it returns specific columns from the file for the designated state and county
def clean_csv(f, sc, cc):
    f_df = pd.read_csv(f)
    f_prime = f_df[['Date Local', '1st Max Value', 'State Code', 
    'County Code', 'Site Num', 'Parameter Code', 'POC', 
    'Sample Duration', 'Method Code']].loc[
    (f_df['State Code'] == sc) & (f_df['County Code'] == cc)]
    return f_prime

all_years = pd.DataFrame()
state_code = 49 # Utah
county_code = 35 # Salt Lake County
pollutant_code = 42101 # Pollutant parameter code for Carbon Monoxide (CO)

for i in range(1980,2021):
    file = file_path + '\\daily_' + str(pollutant_code) + '_' + str(i) + '.csv'
    year = clean_csv(file, state_code, county_code)

    all_years = pd.concat([all_years, year])

all_years.to_csv(file_path + '\\all_years_' + str(pollutant_code) + '_' 
+ str(state_code) + '_' + str(county_code) + '.csv')
\end{python}

After the data had been pulled, some additional cleaning and preparation was needed.
For instance, when pulling the raw data for Salt Lake County, there were many duplicated values by day.
There were three causes of this:
\begin{itemize}
    \item \texttt{Site Num}: multiple measurements across Salt Lake County due to the different monitoring sites.
    \item \texttt{Sample Duration}: different lenghts of time the measurement was aggregated over.
    \item \texttt{POC}: this stands for ``parameter of occurence'', and is a somewhat arbitrary number assigned to different monitoring devices at each station if the station has multiple devices measuring the same pollutant.
\end{itemize}
For duplication due to \texttt{Site Num} and \texttt{POC}, I decided to take the maximum value per day. 
This is a method that follows from the EPA standards on reporting the Air Quality Index (AQI) for an area, which is to report the maximum value for all of the monitors in the area (EPA: Outdoor Air Quality Basic Information).

The duplication over \texttt{Sample Duration} was specific for each pollutant and will be discussed further in the sections below.

One final consideration of the dataset was missing values.
There were instances of single days of missing data, to deal with these, I used the \texttt{interpolation} method from the \texttt{pandas} library in \texttt{python}.
This is a linear method that will only apply to values where there is a single consecutive value missing.
For larger gaps in the data, I took a weighted average of years preceding and following the year with missing values, generally following this formula:
\begin{equation}\label{imp}
    v_{i} = \left(\hat{y}_{i}\right) * 0.3 + \left(\hat{y}_{i+1} + \hat{y}_{i-1}\right) * 0.2 + \left(\hat{y}_{i+2} + \hat{y}_{i-2}\right) * 0.1 + \left(\hat{y}_{i+3} + \hat{y}_{i-3}\right) * 0.05
\end{equation}
Where $v_{i}$ is the imputation value for the missing values of year $i$, and $\hat{y}_{i}$ is the mean for year $i$ of the non-missing values.

Once the imputation was complete, I saved the data into a \texttt{.csv} to be able to import the data into \texttt{R} to begin the statistical analysis.