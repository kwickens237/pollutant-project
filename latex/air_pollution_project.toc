\contentsline {chapter}{Abstract}{ii}{chapter*.2}%
\contentsline {chapter}{Acknowledgements}{iii}{chapter*.3}%
\contentsline {chapter}{List of Figures}{iv}{chapter*.4}%
\contentsline {chapter}{List of Tables}{v}{chapter*.5}%
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Functional data}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Some theoretical results on functional time series}{2}{section.2.1}%
\contentsline {chapter}{\numberline {3}EPA Criteria Pollutants}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}EPA pollutant summary data}{8}{section.3.1}%
\contentsline {section}{\numberline {3.2}Carbon Monoxide}{9}{section.3.2}%
\contentsline {section}{\numberline {3.3}Particulate Matter 2.5}{12}{section.3.3}%
\contentsline {section}{\numberline {3.4}Sulfur Dioxide}{15}{section.3.4}%
\contentsline {section}{\numberline {3.5}Functional data for pollutants}{18}{section.3.5}%
\contentsline {chapter}{\numberline {4}FANOVA}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}Test of means over time}{23}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Carbon Monoxide}{23}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Particulate Matter 2.5}{24}{subsection.4.1.2}%
\contentsline {subsection}{\numberline {4.1.3}Sulfur Dioxide}{24}{subsection.4.1.3}%
\contentsline {section}{\numberline {4.2}Test of means against Denver, CO}{24}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Carbon Monoxide}{25}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Particulate Matter 2.5}{26}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Sulfur Dioxide}{27}{subsection.4.2.3}%
\contentsline {chapter}{Conclusion}{28}{chapter*.21}%
\contentsline {chapter}{Appendix}{29}{chapter*.22}%
\contentsline {chapter}{References}{38}{chapter*.30}%
