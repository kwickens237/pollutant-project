# Title     : Pollutant Data Extraction
# Objective : Read pollutant data from CSV files
# Created by: kaliw
# Created on: 12/14/2019

# Libraries
library(tidyr)
library(dplyr)
library(ggplot2)
library(fda)
library(lubridate)
library(funData)
library(RColorBrewer)
library(plot3D)

# Set working directory
curr_dir <- getwd()
setwd(curr_dir)

# Read in CSV data
CO_data <- read.csv(file = 'C:\\Users\\kaliw\\Documents\\pollutant-project\\data\\pollutant_data\\co_490353006.csv')
CO_slc <- subset(CO_data, sample_duration == '1 HOUR')
CO_slc$date_local <- as.Date(CO_slc$date_local, format = '%m/%d/%Y')
CO_slc$first_max_value <- as.numeric(CO_slc$first_max_value)

head(CO_slc)

# 2000, 2004, 2008, 2012, 2016, 2020 - Leap Years since 1997

CO_slc_sum[CO_slc_sum$date_local == '2000-02-29',]
CO_slc_sum <- CO_slc[!(format(CO_slc$date_local,"%m") == "02" & format(CO_slc$date_local, "%d") == "29"), ,drop = FALSE]

head(CO_slc[(format(CO_slc$date_local,"%m") == "02" & format(CO_slc$date_local, "%d") == "29"),])

CO_slc_sum$day_str <- tolower(strftime(CO_slc_sum$date_local, '%b%d'))
CO_slc_sum$day_str <- factor(CO_slc_sum$day_str, levels = names(dayOfYear))

head(CO_slc_sum)

CO_slc_spread <- as.data.frame(CO_slc_sum) %>%
  select(yr_num, day_str, first_max_value) %>%
  group_by_at(vars(-first_max_value)) %>%
  mutate(row_id = 1:n()) %>%
  ungroup() %>%
  spread(key = yr_num, value = first_max_value) %>%
  select(-row_id)

rownames(CO_slc_spread) <- CO_slc_spread$day_str

CO_slc_spread$day_str <- NULL

basis.fd <- create.fourier.basis(rangeval = c(0,365), nbasis = 15, axes = list("axesIntervals"))
cofd <- Data2fd(dayOfYear, as.matrix(CO_slc_spread), basis.fd)
plot.fd(cofd, main = 'CO Pollutant Functions')

# Subset the data
slc_small <- c('1984', '1988', '1994', '1999', '2004', '2009')
CO_slc_small <- CO_slc_spread[slc_small]

slc_small_fd <- Data2fd(dayOfYear, as.matrix(CO_slc_small), basis.fd)

fun_slc_small <- fd2funData(slc_small_fd, argvals = dayOfYear)

plot(fun_slc_small, col = brewer.pal(n = 6, name = "Dark2"), main = "CO Pollutant Data - Sample")
legend("bottomleft",
legend = slc_small,
col = brewer.pal(n = 6, name = "Dark2"),
lty = 1,
lwd = 1,
cex = 1.5,
bty = "n",
inset = c(0.01, 0.01))

hist3D(z = as.matrix(CO_slc_spread), scale = TRUE, expand = 0.5, bty = "b", phi = 20,
        col = brewer.pal(n = 160, name = "Spectral"), shade = 0.2, ltheta = 90,
        space = 0.4, d = 2)
